/*
 * Copyright © 2015-2017 Roger Leigh <rleigh@codelibre.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of any organization.
 */

#include <algorithm>
#include <cmath>
#include <fstream>
#include <iostream>

#include <boost/range/size.hpp>

#include <codelibre/multiarray/storage.h>
#include <codelibre/multiarray/util.h>

#define CATCH_CONFIG_MAIN
#include <catch.hpp>

#include "print.h"
#include "svg.h"

using codelibre::multiarray::dimension;
using codelibre::multiarray::direction;
using codelibre::multiarray::array_space;
using codelibre::multiarray::vector_space;
using codelibre::multiarray::indexed_storage;
using codelibre::multiarray::indexed_subrange;
using codelibre::multiarray::make_logical_space;

namespace
{

  template<typename Space>
  std::string
  space_name(const std::string& label,
             const Space& space)
  {
    std::ostringstream os;
    os << label;
    for(const auto& dim : space.get_logical_order())
      {
        os << '-';
        os << dim.extent;
      }
    os << '-';
    for(const auto& order : space.storage_order())
      {
        os << '-';
        if (order._direction == direction::ASCENDING)
          os << 'A';
        else
          os << 'D';
      }
    return os.str();
  }


  // Compute all direction permutations for a given number of
  // dimensions.
  std::vector<std::vector<direction>>
  direction_permutations(dimension::index_type count)
  {
    // Combinations of dimension progression directions.
    std::vector<std::vector<direction>> directions;
    {
      for (dimension::index_type i = 0U; i < (1U << count); ++i)
        {
          std::vector<direction> direction;
          for (dimension::index_type j = 0U; j < count; ++j)
            {
              if (!(i & (1 << j)))
                direction.push_back(direction::ASCENDING);
              else
                direction.push_back(direction::DESCENDING);
            }
          directions.push_back(direction);
        }
    }

    return directions;
  }

  // Compute all order permutations (by index) for a given set of
  // dimensions.
  std::vector<std::vector<vector_space::indexed_storage_type> >
  indexed_order_permutations(const std::vector<dimension>& dims)
  {
    auto count = dims.size();

    // Combinations of dimension progression directions.
    auto directions = direction_permutations(count);

    // Combinations of dimension storage ordering.
    std::vector<std::vector<dimension::index_type> > orders;
    {
      std::vector<dimension::index_type> order;
      for(dimension::index_type i = 0; i < count; ++i)
        order.push_back(i);

      do
        {
          orders.push_back(order);
        }
      while(std::next_permutation(order.begin(), order.end()));
    }

    std::vector<std::vector<vector_space::indexed_storage_type> > ret;

    for(const auto& i : orders)
      {
        for(const auto& j : directions)
          {
            std::vector<vector_space::indexed_storage_type> storage;
            for (dimension::index_type k = 0; k < count; ++k)
              storage.push_back(vector_space::indexed_storage_type{i.at(k), j.at(k)});
            ret.push_back(storage);
          }
      }

    return ret;
  }

  // Get all storage order permutations from a dimension list and list
  // of indexed or named storage orders, realised in the form of
  // constructed space objects.
  template<typename Storage>
  std::vector<vector_space>
  order_permutations(std::vector<dimension>             dims,
                     std::vector<std::vector<Storage> > orders)
  {
    {
      vector_space spacedef(dims);

      std::cout << "space default parameters:\n"
                << spacedef;
      //           << "\nspace logical layout:\n\n";
      // dump_array(spacedef, false);
      std::cout << "\nspace default storage layout:\n\n";
      test::dump_array(spacedef);
      std::cout << '\n';
    }

    std::vector<vector_space> ret;

    for(const auto& order : orders)
      ret.push_back(vector_space(dims, order));

    return ret;
  }

  // Test all space permutations provided.  This will ensure
  // round-trip from logical coordinate to storage index and back is
  // working, but does not validate the correctness of the
  // algorithm--it only checks it's behaviour is consistent.  It will
  // also print the array structure and (for subrange sets) the
  // subrange structure in addition to the full range.
  void
  test_orders(const std::string& label,
              std::vector<vector_space> dims)
  {
    for(const auto& dim : dims)
      {
        // Check if subranges are in use, and if so, print the original
        // space as well as the reduced space for visual comparison.
        const vector_space& space(dim);
        const vector_space full_space(make_logical_space(space, false, false));
        const vector_space full_logical_space(make_logical_space(space, false, true));
        const vector_space logical_space(make_logical_space(space, true, true));

        std::cout << space_name(label, space) << '\n';
        std::cout << "space with storage order:\n"
                  << space;
        if (space.num_elements() != full_space.num_elements()) // subrange
          {
            std::cout << "\nspace storage layout (logical):\n\n";
            test::dump_array(full_logical_space);
            std::cout << "\nspace storage layout (actual):\n\n";
            test::dump_array(full_space);
            std::cout << "\nspace storage layout (actual subrange):\n\n";
          }
        else
          {
            std::cout << "\nspace storage layout:\n\n";
          }
        test::dump_array(space);
        {
          std::ofstream svgs(space_name(label, space) + ".svg");
          test::dump_svg(svgs, space);
        }
        std::cout << '\n';

        vector_space::coord_type pos1, pos3;
        for(dimension::index_type i = 0; i < space.num_elements(); ++i)
          {
            logical_space.coord(i, pos1);

            uint32_t pos2 =  space.index(pos1);
            space.coord(pos2, pos3);
            std::cout << pos1
                      << " → " << std::setw(2) << std::right << pos2 << " → "
                      << pos3 << '\n';
            // Check round trip results in same indices.
            REQUIRE(pos1 == pos3);

            // Check the subrange (which may be the full range)
            {
              auto logical = space.get_logical_order();
              vector_space::coord_type full_index = pos1;
              for (std::vector<dimension>::size_type d = 0; d < space.size(); ++d)
                {
                  full_index[d] += logical[d].begin;
                }
              REQUIRE(full_space.index(full_index) == pos2);
            }
          }
        std::cout << '\n';
      }
  }

}

// Dimension construction and failure cases.
TEST_CASE("Dimension/Construct")
{
  dimension x{1U};
  REQUIRE(1U == x.extent);

  dimension t{43U};

  REQUIRE(43U == t.extent);
  REQUIRE(0U == t.begin);
  REQUIRE(43U == t.end);

  // Zero extent size is invalid.
  REQUIRE_THROWS_AS(dimension(0), const std::logic_error&);
}

// Dimension subrange construction and failure cases.
TEST_CASE("Dimension/ConstructSubrange")
{
  dimension x{64};
  REQUIRE(64U == x.extent);
  REQUIRE(64U == x.size());

  dimension x2{x, 16, 48};
  REQUIRE(64U == x2.extent);
  REQUIRE(32U == x2.size());
  REQUIRE(16U == x2.begin);
  REQUIRE(48U == x2.end);

  // Outside full range.
  REQUIRE_THROWS_AS(dimension(64U, 32U, 128U), const std::logic_error&);
  // Outside subrange.
  REQUIRE_THROWS_AS(dimension(x2, 0U, 56U), const std::logic_error&);
}

// space construction and failure cases.
TEST_CASE("Space/Construct")
{
  array_space<3>::logical_order dims
  {{
    {3},
    {4},
    {2}
  }};

  array_space<3> space(dims);

  REQUIRE(3U == space.size());
}

// Round-trip of logical index to logical coordinate.
TEST_CASE("Space/LogicalIndex")
{
  std::vector<dimension> dims
  {
    {3},
    {4},
    {2}
  };

  vector_space space(dims);

  for (vector_space::size_type i = 0; i < space.num_elements(); ++i)
    {
      vector_space::coord_type coord;
      space.coord(i, coord);
      vector_space::size_type i2 = space.index(coord);

      std::cout << std::setw(2) << std::right << i << " → "
                << test::dump_vector<vector_space::coord_type>(coord)
                << " → " << std::setw(2) << std::right << i2 << '\n';

      REQUIRE(i == i2);
    }
}

// Round-trip of logical coordinate to storage index.
TEST_CASE("Space/StorageIndex")
{
  std::vector<dimension> dims
  {
    {3},
    {4},
    {2}
  };

  vector_space space(dims);

  for (vector_space::index_type i = 0; i < space.num_elements(); ++i)
    {
      vector_space::coord_type coord;
      space.coord(i, coord);
      vector_space::index_type i2 = space.index(coord);
      vector_space::index_type i3 = space.index(coord);

      std::cout << std::setw(2) << std::right << i << " → "
                << test::dump_vector<vector_space::coord_type>(coord)
                << " → " << std::setw(2) << std::right << i3 << '\n';

      REQUIRE(i == i2);
      REQUIRE(i == i3);
    }

  // Failure if incorrect length.
  vector_space::coord_type coord{{2U, 1U}};
  REQUIRE_THROWS_AS(space.index(coord), const std::logic_error&);
}

// Storage order failure cases (by index).
TEST_CASE("Space/IndexedStorageOrderFail")
{
  std::vector<dimension> dims{{3}, {4}};

  // Missing dimension.
  std::vector<vector_space::indexed_storage_type> order1{{0, direction::DESCENDING}};
  REQUIRE_THROWS_AS(vector_space(dims, order1), const std::logic_error&);

  // All dimensions correct.
  order1.push_back({1, direction::ASCENDING});
  REQUIRE_NOTHROW(vector_space(dims, order1));

  // Extra duplicated dimension.
  order1.push_back({1, direction::ASCENDING});
  REQUIRE_THROWS_AS(vector_space(dims, order1), const std::logic_error&);

  // Invalid dimension.
  std::vector<vector_space::indexed_storage_type> order2
  {
    {0, direction::DESCENDING},
    {2, direction::DESCENDING}
  };
  REQUIRE_THROWS_AS(vector_space(dims, order2), const std::logic_error&);
}

// Storage ordering (two dimensions by index).
TEST_CASE("Space/StorageOrderIndexedD2")
{
  std::vector<dimension> dims{{3}, {4}};

  // Test all 8 combinations
  test_orders("ID2", order_permutations(dims, indexed_order_permutations(dims)));
}

// Storage ordering (three dimensions by index).
TEST_CASE("Space/StorageOrderD3")
{
  std::vector<dimension> dims{{5}, {4}, {3}};

  // Test all 48 combinations
  test_orders("D3", order_permutations(dims, indexed_order_permutations(dims)));
}

// Storage ordering (nine dimensions by index).
TEST_CASE("Space/StorageOrderD9")
{
  std::vector<dimension> dims
    {
      {10},
      {10},
      {3},
      {2},
      {2},
      {3},
      {2},
      {2},
      {2}
    };

  vector_space s1(dims);

  std::cout << "space s1 default parameters:\n"
            << s1
            << "\nspace s1 default storage layout:\n\n";
  test::dump_array(s1);
  std::cout << '\n';

  std::cout << "s1-1: " << s1 << '\n';
  std::vector<vector_space::indexed_storage_type> order
  {
    {0, direction::ASCENDING},
    {5, direction::DESCENDING},
    {1, direction::ASCENDING},
    {2, direction::ASCENDING},
    {3, direction::ASCENDING},
    {6, direction::ASCENDING},
    {8, direction::ASCENDING},
    {7, direction::DESCENDING},
    {4, direction::ASCENDING}
  };

  vector_space s2(dims, order);

  std::cout << "space s2 modified parameters:\n"
            << s2
            << "\nspace s2 modified storage layout:\n\n";
  test::dump_array(s2);
  std::cout << '\n';
}

// Subrange failure cases (by index).
TEST_CASE("Space/IndexedSubrangeFail")
{
  std::vector<dimension> dims{{16}, {32}};

  vector_space::indexed_subrange_list subrange1;
  subrange1.push_back({0, 4, 12});

  // Missing dimension is OK.
  REQUIRE_NOTHROW(vector_space(dims).subrange(subrange1));

  subrange1.push_back({1, 24, 32});

  // All dimensions present.
  REQUIRE_NOTHROW(vector_space(dims).subrange(subrange1));

  subrange1.push_back({1, 28, 32});

  // Extra duplicated dimension.
  REQUIRE_THROWS_AS(vector_space(dims).subrange(subrange1), const std::logic_error&);

  vector_space::indexed_subrange_list subrange2{{0, 4, 12}, {2, 90, 12}};

  // Invalid dimension.
  REQUIRE_THROWS_AS(vector_space(dims).subrange(subrange2), const std::logic_error&);
}

// Subrange storage ordering (one dimension by index).
TEST_CASE("Space/SubrangeIndexedD1")
{
  std::vector<dimension> dims{{12}};

  std::vector<vector_space> spaces(order_permutations(dims, indexed_order_permutations(dims)));
  vector_space::indexed_subrange_list subrange{{0, 5, 9}};

  std::vector<vector_space> subrange_spaces;
  for(const auto& space : spaces)
    subrange_spaces.push_back(space.subrange(subrange));

  test_orders("ID1.1", subrange_spaces);

  vector_space::indexed_subrange_list subrange2;
  subrange2.push_back({0, 1, 3});

  std::vector<vector_space> subrange_spaces2;
  for(auto i = subrange_spaces.begin();
      i != subrange_spaces.end();
      ++i)
    {
      subrange_spaces2.push_back(i->subrange(subrange2));
    }

  test_orders("ID1", subrange_spaces2);
}

// Subrange storage ordering (two dimensions by index).
TEST_CASE("Space/SubrangeIndexedD2")
{
  std::vector<dimension> dims{{3}, {4}};

  std::vector<vector_space> spaces(order_permutations(dims, indexed_order_permutations(dims)));
  vector_space::indexed_subrange_list subrange{{0, 0, 2}, {1, 1, 3}};

  std::vector<vector_space> subrange_spaces;
  for(const auto& space : spaces)
    subrange_spaces.push_back(space.subrange(subrange));

  test_orders("ID2", subrange_spaces);
}

// Subrange storage ordering (four dimensions by index).
TEST_CASE("Space/SubrangeIndexedD4")
{
  std::vector<dimension> dims{{{8}, {8}, {3}, {2}}};

  std::vector<vector_space> spaces{order_permutations(dims, indexed_order_permutations(dims))};
  vector_space::indexed_subrange_list subrange{{0, 0, 8}, {1, 3, 6}, {2, 0, 2}, {3, 1, 2}};

  std::vector<vector_space> subrange_spaces;
  for(const auto& space : spaces)
    subrange_spaces.push_back(space.subrange(subrange));

  test_orders("ID4.1", subrange_spaces);

  vector_space::indexed_subrange_list subrange2{{0, 4, 7}, {1, 1, 3}};

  std::vector<vector_space> subrange_spaces2;
  for(const auto& space : spaces)
    subrange_spaces2.push_back(space.subrange(subrange).subrange(subrange2));

  test_orders("ID4.2", subrange_spaces2);
}
